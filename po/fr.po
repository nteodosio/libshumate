# French translation for libshumate.
# Copyright (C) 2022 libshumate's COPYRIGHT HOLDER
# This file is distributed under the same license as the libshumate package.
# Alexandre Franke <afranke@gnome.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: libshumate main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libshumate/issues\n"
"POT-Creation-Date: 2022-09-06 18:11+0000\n"
"PO-Revision-Date: 2022-09-09 11:39+0200\n"
"Last-Translator: Alexandre Franke <afranke@gnome.org>\n"
"Language-Team: GNOME French Team <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Gtranslator 40.0\n"

#. m is the unit for meters
#: shumate/shumate-scale.c:197
#, c-format
msgid "%d m"
msgstr "%d m"

#. km is the unit for kilometers
#: shumate/shumate-scale.c:200
#, c-format
msgid "%d km"
msgstr "%d km"

#. ft is the unit for feet
#: shumate/shumate-scale.c:206
#, c-format
msgid "%d ft"
msgstr "%d ft"

#. mi is the unit for miles
#: shumate/shumate-scale.c:209
#, c-format
msgid "%d mi"
msgstr "%d mi"
